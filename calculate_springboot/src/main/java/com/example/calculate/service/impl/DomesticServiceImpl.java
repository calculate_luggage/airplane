package com.example.calculate.service.impl;

import com.example.calculate.mapper.LuggageMapper;
import com.example.calculate.mapper.PassengerMapper;
import com.example.calculate.pojo.Luggage;
import com.example.calculate.pojo.Passenger;
import com.example.calculate.service.DomesticService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.example.calculate.constants.Constants.OVERWEIGHT_FARE_RATIO;

/**
 * 国内行李托运
 */
@Slf4j
@Component
public class DomesticServiceImpl implements DomesticService {

    @Autowired
    private LuggageMapper luggageMapper;

    @Autowired
    private PassengerMapper passengerMapper;

    @Autowired
    private PassengerServiceImpl passengerService;

    @Autowired
    private LuggageServiceImpl luggageService;
    /**
     * 计算一件普通行李的价格
     * @param passenger
     * @param ordinaryLuggage
     * @return
     */
    @Override
    public double ordinaryLuggage(Passenger passenger, List<Luggage> ordinaryLuggage) {
        double luggagePrice = passengerMapper.selectById(passenger.getPassId()).getLuggagePrice();

        log.info("luggagePrice为：" + luggagePrice);

        // 行李总重量
        double totalWeight = luggageService.totalWeight(ordinaryLuggage);

        if (passenger.getPassType() == 1) {
            if (passenger.getSeatType() < 3) {
                if (passenger.getMembershipType() == 0) {
                    if (totalWeight > 20) {
                        luggagePrice += (totalWeight - 20) * (passenger.getEconomyTicketPrice() * OVERWEIGHT_FARE_RATIO);
                    }
                } else if (passenger.getMembershipType() == 1 || passenger.getMembershipType() == 2) {
                    if (totalWeight > 40) {
                        luggagePrice += (totalWeight - 40) * (passenger.getEconomyTicketPrice() * OVERWEIGHT_FARE_RATIO);
                    }

                } else if (passenger.getMembershipType() == 3) {
                    if (totalWeight > 50) {
                        luggagePrice += (totalWeight - 50) * (passenger.getEconomyTicketPrice() * OVERWEIGHT_FARE_RATIO);
                    }
                }
            } else if (passenger.getSeatType() == 3) {
                if (passenger.getMembershipType() == 0) {
                    if (totalWeight > 30) {
                        luggagePrice += (totalWeight - 30) * (passenger.getEconomyTicketPrice() * OVERWEIGHT_FARE_RATIO);
                    }

                } else if (passenger.getMembershipType() == 1 || passenger.getMembershipType() == 2) {
                    if (totalWeight > 50) {
                        luggagePrice += (totalWeight - 50) * (passenger.getEconomyTicketPrice() * OVERWEIGHT_FARE_RATIO);
                    }

                } else if (passenger.getMembershipType() == 3) {
                    if (totalWeight > 60) {
                        luggagePrice += (totalWeight - 60) * (passenger.getEconomyTicketPrice() * OVERWEIGHT_FARE_RATIO);
                    }
                }
            } else if (passenger.getSeatType() == 4) {
                if (passenger.getMembershipType() == 0) {
                    if (totalWeight > 40) {
                        luggagePrice += (totalWeight - 40) * (passenger.getEconomyTicketPrice() * OVERWEIGHT_FARE_RATIO);
                    }

                } else if (passenger.getMembershipType() == 1 || passenger.getMembershipType() == 2) {
                    if (totalWeight > 60) {
                        luggagePrice += (totalWeight - 60) * (passenger.getEconomyTicketPrice() * OVERWEIGHT_FARE_RATIO);
                    }

                } else if (passenger.getMembershipType() == 3) {
                    if (totalWeight > 70) {
                        luggagePrice += (totalWeight - 70) * (passenger.getEconomyTicketPrice() * OVERWEIGHT_FARE_RATIO);
                    }
                }
            }

        } else {
            if (passenger.getMembershipType() == 0) {
                if (totalWeight > 10) {
                    luggagePrice += (totalWeight - 10) * (passenger.getEconomyTicketPrice() * OVERWEIGHT_FARE_RATIO);
                }

            } else if (passenger.getMembershipType() == 1 || passenger.getMembershipType() == 2) {
                if (totalWeight > 30) {
                    luggagePrice += (totalWeight - 30) * (passenger.getEconomyTicketPrice() * OVERWEIGHT_FARE_RATIO);
                }

            } else if (passenger.getMembershipType() == 3) {
                if (totalWeight > 40) {
                    luggagePrice += (totalWeight - 40) * (passenger.getEconomyTicketPrice() * OVERWEIGHT_FARE_RATIO);
                }
            }
        }
        passenger.setLuggagePrice(luggagePrice);
        //这里不用更新数据库，只需要返回当前的luggagePrice
//        passengerMapper.updateById(passenger);
        return luggagePrice;
    }


    /**
     * 国内航班乘客托运行李价格计算
     *
     * @param passenger
     */
    @Override
    public void totalDomestic(Passenger passenger) {

        //每一次算一个乘客的行李价格要先对数据库置0;
        passengerService.updatePriceById(passenger.getPassId());
        Passenger passenger1 = passengerMapper.selectById(passenger.getPassId());
        double luggagePrice = passenger1.getLuggagePrice();

        // 计算该乘客满足条件的行李
        // TODO 特殊行李是可以超过32kg的
        List<Luggage> luggageList1 = luggageService.specialLuggageLimit(passenger);
        List<Luggage> specialLuggage = luggageService.specialLuggage(luggageList1);

        List<Luggage> luggageList = luggageService.luggageLimit(passenger);
        List<Luggage> ordinaryLuggage = luggageService.ordinaryLuggage(luggageList);

        // 普通行李

        log.info("Processing ordinary luggage.");
        luggagePrice += ordinaryLuggage(passenger, ordinaryLuggage);

        // 特殊行李
        log.info("Processing special luggage.");
        luggagePrice += domesticSpecial(passenger, specialLuggage);

        passenger.setLuggagePrice(luggagePrice);
        passengerMapper.updateById(passenger);
    }

    /**
     * 国内特殊行李价格计算
     * @param passenger
     * @param domesticSpecial
     * @return
     */
    @Override
    public double domesticSpecial(Passenger passenger, List<Luggage> domesticSpecial) {
        double luggagePrice = passengerMapper.selectById(passenger.getPassId()).getLuggagePrice();

        for(Luggage luggage : domesticSpecial) {
            if(luggage.getSpecialType() == 0) {
                return luggagePrice;
            } // 这里的特殊行李都安装经济票价1.5%计算
            else if(luggage.getSpecialType() == 2 || luggage.getSpecialType() == 3 || (luggage.getSpecialType() > 4 && luggage.getSpecialType() < 9)) {
                luggagePrice += luggage.getWeight() * (passenger.getEconomyTicketPrice() * OVERWEIGHT_FARE_RATIO);
            } else {
                log.error("没有这个类型的特殊行李");
            }
        }
        passenger.setLuggagePrice(luggagePrice);

        return luggagePrice;
    }


}
