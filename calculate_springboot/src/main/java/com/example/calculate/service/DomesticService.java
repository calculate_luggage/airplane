package com.example.calculate.service;

import com.example.calculate.pojo.Luggage;
import com.example.calculate.pojo.Passenger;

import java.util.List;

public interface DomesticService {

    /**
     * 国内普通行李价格
     * @param passenger
     * @return
     */
    public double ordinaryLuggage(Passenger passenger, List<Luggage> ordinaryLuggage);

    /**
     * 国内总普通行李价格
     *
     * @param passenger
     */
    public void totalDomestic(Passenger passenger);

    /**
     * 国内特殊行李价格计算
     *
     * @param passenger
     * @param luggageList
     * @return
     */
    public double domesticSpecial(Passenger passenger, List<Luggage> luggageList);




}
