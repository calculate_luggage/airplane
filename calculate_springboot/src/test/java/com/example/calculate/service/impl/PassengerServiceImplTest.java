package com.example.calculate.service.impl;

import com.example.calculate.mapper.PassengerMapper;
import com.example.calculate.pojo.Passenger;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class PassengerServiceImplTest {

    @Autowired
    private PassengerServiceImpl passengerService;

    @Autowired
    private PassengerMapper passengerMapper;

    @Test
    void updatePriceById() {
        Long passId = 1L;
        passengerService.updatePriceById(passId);
        Passenger passenger = passengerMapper.selectById(1);
        if(passenger.getLuggagePrice() == 0) {
            System.out.println("---------LuggagePrice置0成功-----");
        }
    }
}