package com.example.calculate.service.impl;

import com.example.calculate.mapper.LuggageMapper;
import com.example.calculate.mapper.PassengerMapper;
import com.example.calculate.pojo.Luggage;
import com.example.calculate.pojo.Passenger;
import com.example.calculate.service.LuggageService;
import lombok.Builder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
class DomesticServiceImplTest {

    @Autowired
    private LuggageMapper luggageMapper;

    @Autowired
    private PassengerMapper passengerMapper;

    @Autowired
    private LuggageServiceImpl luggageService;

    @Autowired
    private DomesticServiceImpl domesticService;

    @Test
    void ordinaryLuggage() {
        Passenger passenger = Passenger.builder()
                .economyTicketPrice(666)
                .luggagePrice(0)
                .area(0)
                .membershipType(1)
                .passId(1L)
                .passType(1)
                .seatType(1).build();
        System.out.println(passenger);

        Luggage luggage = Luggage.builder()
                .luggageId(1L)
                .height(34)
                .length(45)
                .passId(1L)
                .type(0)
                .weight(23)
                .width(56)
                .build();
        Luggage luggage2 = Luggage.builder()
                .luggageId(1L)
                .height(34)
                .length(67)
                .passId(1L)
                .type(0)
                .weight(27)
                .width(56)
                .build();

        List<Luggage> luggageList = new ArrayList<>();
        luggageList.add(luggage);
        luggageList.add(luggage2);
        luggageList.stream().forEach(luggage1 -> System.out.println(luggage1));
        double luggagePrice = domesticService.ordinaryLuggage(passenger, luggageList);
        System.out.println("ordinaryLuggage的测试结果为" + luggagePrice);

    }

    @Test
    void totalDomestic() {
        Passenger passenger = passengerMapper.selectById(1);
        System.out.println(passenger);

        List<Luggage> luggageList = luggageService.getLuggageList(passenger);
        luggageList.stream().forEach(luggage1 -> System.out.println(luggage1));

        domesticService.totalDomestic(passenger);
    }
}