package com.example.calculate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.calculate.pojo.Passenger;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

@Component
public interface PassengerMapper extends BaseMapper<Passenger> {
}
