package com.example.calculate.service.impl;

import com.example.calculate.mapper.LuggageMapper;
import com.example.calculate.mapper.PassengerMapper;
import com.example.calculate.pojo.Luggage;
import com.example.calculate.pojo.Passenger;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@SpringBootTest
class ForeignServiceImplTest {
    @Autowired
    private DomesticServiceImpl domesticService;

    @Autowired
    private ForeignServiceImpl foreignService;

    @Autowired
    private LuggageMapper luggageMapper;

    @Autowired
    private PassengerServiceImpl passengerService;

    @Autowired
    private PassengerMapper passengerMapper;

    @Autowired
    private LuggageServiceImpl luggageService;

    @Test
    void areaOneOverweight() {
    }

    @Test
    void areaOne() {
        Passenger passenger = passengerMapper.selectById(2);
        foreignService.areaOne(passenger);
        System.out.println(passenger);
        System.out.println(luggageService.getLuggageList(passenger));
        double luggagePrice = passenger.getLuggagePrice();
        log.info("-----------testAreaOne-------------");
        System.out.println("testAreaOne的价格是：" + luggagePrice);
        log.info("-----------testAreaOne-------------");
    }

    @Test
    void areaTwoOverweight() {
        Passenger passenger = Passenger.builder()
                .economyTicketPrice(666)
                .luggagePrice(0)
                .area(0)
                .membershipType(1)
                .passId(1L)
                .passType(1)
                .seatType(1).build();
        System.out.println(passenger);

        Luggage luggage = Luggage.builder()
                .luggageId(1L)
                .height(34)
                .length(45)
                .passId(1L)
                .specialType(1)
                .type(1)
                .weight(23)
                .width(56)
                .build();
        Luggage luggage2 = Luggage.builder()
                .luggageId(1L)
                .height(34)
                .length(67)
                .passId(1L)
                .specialType(1)
                .type(1)
                .weight(27)
                .width(56)
                .build();

        List<Luggage> luggageList = new ArrayList<>();
        luggageList.add(luggage);
        luggageList.add(luggage2);
        System.out.println(passenger);
        luggageList.stream().forEach(luggage1 -> System.out.println(luggage1));
        double luggagePrice = foreignService.areaTwoOverweight(passenger, luggageList);
        System.out.println("areaTwoOverweight的测试结果为" + luggagePrice);
    }

    @Test
    void areaTwo() {
        Passenger passenger = passengerMapper.selectById(1);
        foreignService.areaTwo(passenger);
        double luggagePrice = passenger.getLuggagePrice();
        log.info("-----------testAreaTwo-------------");
        System.out.println("testAreaTwo：" + luggagePrice);
        log.info("-----------testAreaTwo-------------");
    }

    @Test
    void areaThreeOverweight() {
        Passenger passenger = Passenger.builder()
                .economyTicketPrice(666)
                .luggagePrice(0)
                .area(0)
                .membershipType(1)
                .passId(1L)
                .passType(1)
                .seatType(1).build();
        System.out.println(passenger);

        Luggage luggage = Luggage.builder()
                .luggageId(1L)
                .height(34)
                .length(45)
                .passId(1L)
                .specialType(1)
                .type(1)
                .weight(23)
                .width(56)
                .build();
        Luggage luggage2 = Luggage.builder()
                .luggageId(1L)
                .height(34)
                .length(67)
                .passId(1L)
                .specialType(1)
                .type(1)
                .weight(27)
                .width(56)
                .build();

        List<Luggage> luggageList = new ArrayList<>();
        luggageList.add(luggage);
        luggageList.add(luggage2);
        double luggagePrice = foreignService.areaThreeOverweight(passenger, luggageList);
        System.out.println("areaThreeOverweight的测试结果为" + luggagePrice);
    }

    @Test
    void areaThree() {
        Passenger passenger = passengerMapper.selectById(1);
        foreignService.areaThree(passenger);
        double luggagePrice = passenger.getLuggagePrice();
        log.info("-----------testAreaThree-------------");
        System.out.println("testAreaThree：" + luggagePrice);
        log.info("-----------testAreaThree-------------");
    }

    @Test
    void areaFourOverweight() {
        Passenger passenger = Passenger.builder()
                .economyTicketPrice(666)
                .luggagePrice(0)
                .area(0)
                .membershipType(1)
                .passId(1L)
                .passType(1)
                .seatType(1).build();
        System.out.println(passenger);

        Luggage luggage = Luggage.builder()
                .luggageId(1L)
                .height(34)
                .length(45)
                .passId(1L)
                .specialType(1)
                .type(1)
                .weight(23)
                .width(56)
                .build();
        Luggage luggage2 = Luggage.builder()
                .luggageId(1L)
                .height(34)
                .length(67)
                .passId(1L)
                .specialType(1)
                .type(1)
                .weight(27)
                .width(56)
                .build();

        List<Luggage> luggageList = new ArrayList<>();
        luggageList.add(luggage);
        luggageList.add(luggage2);
        double luggagePrice = foreignService.areaFourOverweight(passenger, luggageList);
        System.out.println("areaFourOverweight的测试结果为" + luggagePrice);
    }

    @Test
    void areaFour() {
        Passenger passenger = passengerMapper.selectById(1);
        foreignService.areaFour(passenger);
        double luggagePrice = passenger.getLuggagePrice();
        log.info("-----------testAreaFour-------------");
        System.out.println("testAreaFour：" + luggagePrice);
        log.info("-----------testAreaFour-------------");
    }

    @Test
    void areaFiveOverweight() {
        Passenger passenger = Passenger.builder()
                .economyTicketPrice(666)
                .luggagePrice(0)
                .area(0)
                .membershipType(1)
                .passId(1L)
                .passType(1)
                .seatType(1).build();
        System.out.println(passenger);

        Luggage luggage = Luggage.builder()
                .luggageId(1L)
                .height(34)
                .length(45)
                .passId(1L)
                .specialType(1)
                .type(1)
                .weight(23)
                .width(56)
                .build();
        Luggage luggage2 = Luggage.builder()
                .luggageId(1L)
                .height(34)
                .length(67)
                .passId(1L)
                .specialType(1)
                .type(1)
                .weight(27)
                .width(56)
                .build();

        List<Luggage> luggageList = new ArrayList<>();
        luggageList.add(luggage);
        luggageList.add(luggage2);
        double luggagePrice = foreignService.areaFiveOverweight(passenger, luggageList);
        System.out.println("areaFiveOverweight的测试结果为" + luggagePrice);
    }

    @Test
    void areaFive() {
        Passenger passenger = passengerMapper.selectById(1);
        foreignService.areaFive(passenger);
        double luggagePrice = passenger.getLuggagePrice();
        log.info("-----------testAreaFive-------------");
        System.out.println("testAreaFive：" + luggagePrice);
        log.info("-----------testAreaFive-------------");
    }
}