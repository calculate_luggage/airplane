package com.example.calculate;

import com.example.calculate.mapper.LuggageMapper;
import com.example.calculate.mapper.PassengerMapper;
import com.example.calculate.pojo.Luggage;
import com.example.calculate.pojo.Passenger;
import com.example.calculate.service.impl.DomesticServiceImpl;
import com.example.calculate.service.impl.ForeignServiceImpl;
import com.example.calculate.service.impl.LuggageServiceImpl;
import com.example.calculate.service.impl.PassengerServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@Slf4j
@SpringBootTest
class CalculateApplicationTests {

    @Autowired
    private DomesticServiceImpl domesticService;

    @Autowired
    private ForeignServiceImpl foreignService;

    @Autowired
    private LuggageMapper luggageMapper;

    @Autowired
    private PassengerServiceImpl passengerService;

    @Autowired
    private PassengerMapper passengerMapper;

    @Autowired
    private LuggageServiceImpl luggageService;
    /**
     * 测试国内行李价格
     */
    @Test
    void contextLoads() {

        System.out.println(("----- selectAll method test ------"));


        Passenger passenger = passengerMapper.selectById(1);
        System.out.println(passenger);
        domesticService.totalDomestic(passenger);
        double price = passengerMapper.selectById(1).getLuggagePrice();
        log.info("测试得出的price:" + price);

//        QueryWrapper<Luggage> queryWrapper = Wrappers.query();
//        queryWrapper.eq("pass_id", 1);
//        List<Luggage> luggageList = luggageMapper.selectList(queryWrapper);

//        QueryWrapper<Passenger> queryWrapper2 = Wrappers.query();
//        queryWrapper2.eq("pass_id", 1);
//        List<Passenger> passenger = passengerMapper.selectList(queryWrapper2);
//        passenger.forEach(System.out::println);
//        luggageList.forEach(System.out::println);


    }

    @Test
    void testClassifyLuggage(){

        Passenger passenger = passengerMapper.selectById(1);
        List<Luggage> luggageList = luggageService.getLuggageList(passenger);
        List<Luggage> ordinaryLuggage = luggageService.ordinaryLuggage(luggageList);
        List<Luggage> specialLuggage = luggageService.specialLuggage(luggageList);
        log.info("-----------luggageList-------------");
        luggageList.forEach(System.out::println);
        log.info("-----------ordinaryLuggage-------------");
        ordinaryLuggage.forEach(System.out::println);
        log.info("-----------specialLuggage-------------");
        specialLuggage.forEach(System.out::println);
    }

    @Test
    void testDomesticPrice() {
        Passenger passenger = passengerMapper.selectById(1);
        domesticService.totalDomestic(passenger);
    }

    /**
     * 剔除满足条件的行李
     */
    @Test
    void testRemoveLuggage() {
        Passenger passenger = passengerMapper.selectById(1);
        List<Luggage> luggageList = luggageService.luggageLimit(passenger);
        List<Luggage> reomveFreeLuggage = luggageService.reomveFreeLuggage(luggageList, 2, 32);
        log.info("-----------reomveFreeLuggage-------------");
        System.out.println(reomveFreeLuggage.size());
        reomveFreeLuggage.forEach(System.out::println);
    }

    @Test
    void testAreaOne() {
        Passenger passenger = passengerMapper.selectById(1);
        foreignService.areaOne(passenger);
        double luggagePrice = passenger.getLuggagePrice();
        log.info("-----------testAreaOne-------------");
        System.out.println("testAreaOne的价格是：" + luggagePrice);
        log.info("-----------testAreaOne-------------");

    }

    /**
     * 测试置0
     */
    @Test
    void testSetZero() {
        Passenger passenger = passengerMapper.selectById(1);
        double luggagePrice = passenger.getLuggagePrice();
        log.info("置0之前的luggagePrice为：" + luggagePrice);

        passengerService.updatePriceById(1L);
        Passenger passenger1 = passengerMapper.selectById(1);
        double luggagePrice1 = passenger1.getLuggagePrice();
        log.info("置0之后的luggagePrice为：" + luggagePrice1);

    }

    @Test
    void testAreaTwo() {
        Passenger passenger = passengerMapper.selectById(1);
        double luggagePrice = passenger.getLuggagePrice();
        log.info("置0之前的luggagePrice为：" + luggagePrice);

        passengerService.updatePriceById(1L);
        Passenger passenger1 = passengerMapper.selectById(1);
        double luggagePrice1 = passenger1.getLuggagePrice();
        log.info("置0之后的luggagePrice为：" + luggagePrice1);

    }
}
