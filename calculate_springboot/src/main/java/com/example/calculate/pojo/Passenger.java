package com.example.calculate.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@TableName("`passenger`")
@Builder
public class Passenger {
    @TableId
    private Long passId;

    private double economyTicketPrice;

    // 0代表国内航班，1代表区域1，2代表区域2，3代表区域三，4代表区域四，5代表区域五
    private Integer area;

    // 0表示婴儿，1表示成人/儿童
    private Integer passType;

    // 0表示经济舱，1表示悦享经济舱，2表示超级经济舱，3表示公务舱，4表示头等舱
    private Integer seatType;

    // 0表示普通会员，1表示星空联盟金卡， 2表示凤凰知音金卡、银卡，3表示凤凰知音终身白金卡、白金卡
    private Integer membershipType;

    private double luggagePrice;

}
