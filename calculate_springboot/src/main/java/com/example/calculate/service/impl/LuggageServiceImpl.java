package com.example.calculate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.calculate.mapper.LuggageMapper;
import com.example.calculate.mapper.PassengerMapper;
import com.example.calculate.pojo.Luggage;
import com.example.calculate.pojo.Passenger;
import com.example.calculate.service.LuggageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
public class LuggageServiceImpl implements LuggageService {
    @Autowired
    private LuggageMapper luggageMapper;

    @Autowired
    private PassengerMapper passengerMapper;

    /**
     * 特殊行李限制
     *
     * @param passenger
     * @return
     */
    @Override
    public List<Luggage> specialLuggageLimit(Passenger passenger){
        List<Luggage> luggageList = getLuggageList(passenger);

        List<Luggage> update = new ArrayList<>();

        for(Luggage luggage : luggageList) {
            if(luggage.getWeight() >= 2 && luggage.getWeight() <= 45) {
                update.add(luggage);
            } else {
                log.error("你的尺寸或重量不符合要求");
            }
        }
        return update;
    }

    /**
     * 计算特殊行李部分
     * @param passenger
     * @param specialLuggage
     * @return
     */
    // TODO 国内和国外不能使用同一种特殊行李计算方法
    @Override
    public double specialLuggage(Passenger passenger, List<Luggage> specialLuggage) {
        double luggagePrice = passengerMapper.selectById(passenger.getPassId()).getLuggagePrice();

        for(Luggage luggage : specialLuggage) {
            if(luggage.getSpecialType() == 0) {
                return luggagePrice;
            } else if(luggage.getSpecialType() == 2) {
                if(luggage.getWeight() >= 2 && luggage.getWeight() <= 23) {
                    luggagePrice += 2600;
                } else if(luggage.getWeight() > 23 && luggage.getWeight() <= 32) {
                    luggagePrice += 3900;
                } else if(luggage.getWeight() > 32 && luggage.getWeight() <= 45) {
                    luggagePrice += 5200;
                }
            } else if (luggage.getSpecialType() == 3) {
                if(luggage.getWeight() >= 2 && luggage.getWeight() <= 23) {
                    luggagePrice += 1300;
                } else if(luggage.getWeight() > 23 && luggage.getWeight() <= 32) {
                    luggagePrice += 2600;
                } else if(luggage.getWeight() > 32 && luggage.getWeight() <= 45) {
                    luggagePrice += 3900;
                }
            } else if (luggage.getSpecialType() == 5) {
                if(luggage.getWeight() >= 2 && luggage.getWeight() <= 23) {
                    luggagePrice += 490;
                } else if(luggage.getWeight() > 23 && luggage.getWeight() <= 32) {
                    luggagePrice += 3900;
                }
            } else if (luggage.getSpecialType() == 6) {
                if(luggage.getWeight() >= 2 && luggage.getWeight() <= 23) {
                    luggagePrice += 1300;
                } else if(luggage.getWeight() > 23 && luggage.getWeight() <= 32) {
                    luggagePrice += 2600;
                }
            } else if (luggage.getSpecialType() == 7) {
                if(luggage.getWeight() >= 2 && luggage.getWeight() <= 23) {
                    luggagePrice += 1300;
                }
            } else if (luggage.getSpecialType() == 8) {
                if(luggage.getWeight() >= 2 && luggage.getWeight() <= 5) {
                    luggagePrice += 3900;
                } else if(luggage.getWeight() > 23 && luggage.getWeight() <= 32) {
                    luggagePrice += 5200;
                } else if(luggage.getWeight() > 32 && luggage.getWeight() <= 45) {
                    luggagePrice += 7800;
                }
            } else {
                log.error("没有这个类型的特殊行李");
            }
        }
        passenger.setLuggagePrice(luggagePrice);

        return luggagePrice;
    }

    /**
     * 查询该乘客的所有行李
     * @param passenger
     * @return
     */
    public List<Luggage> getLuggageList(Passenger passenger){
        QueryWrapper<Luggage> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("pass_id", passenger.getPassId());
        List<Luggage> luggageList = luggageMapper.selectList(queryWrapper);
        return luggageList;
    }

    /**
     * 得到行李总重量
     * @param luggageList
     * @return
     */
    @Override
    public double totalWeight(List<Luggage> luggageList) {
        double totalWeight = 0;
        for(Luggage luggage : luggageList) {
            totalWeight += luggage.getWeight();
        }
        return totalWeight;
    }

    /**
     * 得到特殊行李
     * @param luggageList
     * @return
     */
    @Override
    public List<Luggage> specialLuggage(List<Luggage> luggageList) {
        List<Luggage> update = new ArrayList<>();

        // TODO 这里如果用remove会报错
        for(Luggage luggage : luggageList) {
            if(luggage.getType() == 1 && luggage.getSpecialType() != 1 && luggage.getSpecialType() != 4) {
                update.add(luggage);
            }
        }
        return update;
    }

//    public List<Luggage> foreignSpecial(List<Luggage> luggageList) {
//        List<Luggage> update = new ArrayList<>();
//
//        //
//        for(Luggage luggage : luggageList) {
//            if(luggage.getType() == 1 && luggage.getSpecialType() != null) {
//                update.add(luggage);
//            }
//        }
//        return update;
//    }

    /**
     * 得到普通行李
     * @param luggageList
     * @return
     */
    @Override
    public List<Luggage> ordinaryLuggage(List<Luggage> luggageList) {
        List<Luggage> update = new ArrayList<>();

        for(Luggage luggage : luggageList) {
            if(luggage.getType() == 0 || luggage.getSpecialType() == 1 || luggage.getSpecialType() == 4) {
                update.add(luggage);
            }
        }
        return update;
    }

    /**
     * 得到满足条件的行李，尺寸，重量限制
     * @param passenger
     * @return
     */

    @Override
    public List<Luggage> luggageLimit(Passenger passenger) {
        List<Luggage> luggageList = getLuggageList(passenger);

        List<Luggage> update = new ArrayList<>();

        for(Luggage luggage : luggageList) {
            double totalSize = luggage.getHeight() + luggage.getWidth() + luggage.getLength();
            if((totalSize >= 60 && totalSize <= 203) && (luggage.getWeight() >= 2 && luggage.getWeight() <= 32)) {
                update.add(luggage);
            } else {
                log.error("你的尺寸或重量不符合要求");
            }
        }
        return update;
    }

    /**
     * 剔除满足要求的最大尺寸的两个行李，得到满足重量和尺寸要求的行李列表来计算超出免费托运的行李价格
     * @param luggageList
     * @param freeCount
     * @param weight
     * @return
     */
    // TODO 这里的过滤有问题
    @Override
    public List<Luggage> reomveFreeLuggage(List<Luggage> luggageList, int freeCount, double weight) {
        List<Luggage> result = new ArrayList<>();

        // 如果行李数量小于免费托运数量且每个行李都小于32kg，则直接返回空列表
        if (luggageList.size() < freeCount && luggageList.stream().allMatch(luggage -> luggage.getWeight() < weight)) {
            return result;
        }
        // 过滤出满足要求（重量<weight ）（尺寸>=60 && 尺寸<=203）的行李
        List<Luggage> filteredLuggage = luggageList.stream()
                .filter(luggage -> luggage.getWeight() <= weight &&
                        calculateTotalSize(luggage) >= 60 && calculateTotalSize(luggage) <= 158)
                .limit(freeCount)
                .collect(Collectors.toList());

        return filteredLuggage;
    }

    /**
     * 行李总尺寸计算
     * @param luggage
     * @return
     */
    @Override
    public double calculateTotalSize(Luggage luggage) {
        return luggage.getLength() + luggage.getWidth() + luggage.getHeight();
    }

    /**
     * 得到满足特定条件的行李
     * @param luggageList
     * @param weight
     * @return
     */
    @Override
    public List<Luggage> luggageOver(List<Luggage> luggageList, double weight) {
        List<Luggage> update = new ArrayList<>();

        for(Luggage luggage : luggageList) {
            double totalSize = luggage.getHeight() + luggage.getWidth() + luggage.getLength();
            // 因为我在用这个方法是，输入的列表已经是剔除不满足基本要求的行李
            if(totalSize > 158 || luggage.getWeight() > weight) {
                update.add(luggage);
            } else {
                log.error("你的尺寸或重量没超过要求");
            }
        }
        return update;
    }

    /**
     * 根据行李id删除行李
     * @param luggageId
     */
    @Override
    public void deleteLuggage(Long luggageId) {
        luggageMapper.deleteById(luggageId);
    }

}
