package com.example.calculate.service.impl;

import com.example.calculate.mapper.LuggageMapper;
import com.example.calculate.mapper.PassengerMapper;
import com.example.calculate.pojo.Luggage;
import com.example.calculate.pojo.Passenger;
import com.example.calculate.service.ForeignService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.example.calculate.constants.Constants.OVERWEIGHT_FARE_RATIO;

@Slf4j
@Component
public class ForeignServiceImpl implements ForeignService {

    @Autowired
    private PassengerMapper passengerMapper;

    @Autowired
    private LuggageServiceImpl luggageService;
    @Autowired
    private PassengerServiceImpl passengerService;

    /**
     * 区域一计算普通行李价格（超重或者不超重，行李数量）
     * @param passenger
     * @param luggageList
     * @return
     */
    @Override
    public double areaOneOverweight(Passenger passenger, List<Luggage> luggageList) {
        // 统计没超重超尺寸的行李数量
        double luggagePrice = passengerMapper.selectById(passenger.getPassId()).getLuggagePrice();
        // TODO 这里的各种舱位又不一样

        if(passenger.getMembershipType() > -1 && passenger.getMembershipType() < 3) {
            if(luggageList.size() == 0) {
                luggagePrice += 0;
            } else if (luggageList.size() == 1) {
                luggagePrice += 1400;
            } else if (luggageList.size() == 2) {
                luggagePrice += 3400;
            } else if (luggageList.size() > 3) {
                luggagePrice += (3400 + 3000 * (luggageList.size() - 2));
            }

            for(Luggage luggage : luggageList) {
                double totalSize = luggage.getHeight() + luggage.getWidth() + luggage.getLength();

                if ((luggage.getWeight() > 23 && luggage.getWeight() <= 28) && totalSize <= 158) {
                    luggagePrice += 380;
                } else if (luggage.getWeight() > 28 && totalSize <= 158) {
                    luggagePrice += 980;
                } else if (luggage.getWeight() <= 23 && totalSize > 158) {
                    luggagePrice += 980;
                } else if (luggage.getWeight() > 23 && totalSize > 158) {
                    luggagePrice += 1400;
                }
            }

        } else if(passenger.getMembershipType() > 2 && passenger.getMembershipType() < 5) {
            if(luggageList.size() == 0) {
                luggagePrice += 0;
            } else if (luggageList.size() == 1) {
                luggagePrice += 1400;
            } else if (luggageList.size() == 2) {
                luggagePrice += 3400;
            } else if (luggageList.size() > 3) {
                luggagePrice += (3400 + 3000 * (luggageList.size() - 2));
            }

            for(Luggage luggage : luggageList) {
                double totalSize = luggage.getHeight() + luggage.getWidth() + luggage.getLength();

                if(totalSize > 158 && totalSize <= 203) {
                    luggagePrice += 980;
                }
            }
        } else {
            log.error("没有这个会员类型");
        }

        passenger.setLuggagePrice(luggagePrice);
        return luggagePrice;
    }

    /**
     * 区域一行李总价格计算
     * @param passenger
     */
    @Override
    public void areaOne(Passenger passenger) {
        //每一次算一个乘客的行李价格要先对数据库置0;
        passengerService.updatePriceById(passenger.getPassId());

        Passenger passenger1 = passengerMapper.selectById(passenger.getPassId());
        double luggagePrice = passenger1.getLuggagePrice();

        // 计算该乘客满足条件的行李
        List<Luggage> luggageList3 = luggageService.specialLuggageLimit(passenger);
        List<Luggage> specialLuggage = luggageService.specialLuggage(luggageList3);

        List<Luggage> luggageList = luggageService.luggageLimit(passenger);
        List<Luggage> ordinaryLuggage = luggageService.ordinaryLuggage(luggageList);

        // 普通行李
        if(passenger.getPassType() == 0) {
            if(passenger.getSeatType() < 3) {
                if(passenger.getMembershipType() == 0) {
                    luggagePrice += areaOneOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 1, 23));
                } else if(passenger.getMembershipType() > 0 && passenger.getMembershipType() < 4) {
                    luggagePrice += areaOneOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 2, 23));
                } else {
                    log.error("没有这个会员类型");
                }
            } else if(passenger.getSeatType() >= 3) {
                if(passenger.getMembershipType() == 0) {
                    luggagePrice += areaOneOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 1, 23));
                } else if (passenger.getMembershipType() == 1) {
                    luggagePrice += areaOneOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 2, 23));
                } else if(passenger.getMembershipType() ==2 && passenger.getMembershipType() == 3) {
                    List<Luggage> luggageList1 = luggageService.reomveFreeLuggage(ordinaryLuggage, 1, 23);
                    List<Luggage> luggageList2 = luggageService.reomveFreeLuggage(ordinaryLuggage, 1, 32);
                    luggagePrice += areaOneOverweight(passenger, luggageList2);
                } else {
                    log.error("没有这个会员类型");
                }
            } else {
                log.error("没有这个座位类型");
            }
        } else if(passenger.getPassType() == 1) {
            if(passenger.getSeatType() == 0) {
                if(passenger.getMembershipType() == 0) {
                    luggagePrice += areaOneOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 2, 23));
                } else if(passenger.getMembershipType() > 0 && passenger.getMembershipType() < 4) {
                    luggagePrice += areaOneOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 3, 23));
                } else {
                    log.error("没有这个会员类型");
                }
            } else if(passenger.getSeatType() == 1 || passenger.getSeatType() == 2) {
                if(passenger.getMembershipType() == 0) {
                    luggagePrice += areaOneOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 2, 23));
                } else if(passenger.getMembershipType() > 0 && passenger.getMembershipType() < 4) {
                    luggagePrice += areaOneOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 2, 23));
                } else {
                    log.error("没有这个会员类型");
                }
            } else if(passenger.getSeatType() == 3 || passenger.getSeatType() == 4) {
                if(passenger.getMembershipType() == 0) {
                    luggagePrice += areaOneOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 2, 32));
                } else if (passenger.getMembershipType() == 1) {
                    List<Luggage> luggageList1 = luggageService.reomveFreeLuggage(ordinaryLuggage, 1, 23);
                    List<Luggage> luggageList2 = luggageService.reomveFreeLuggage(ordinaryLuggage, 2, 32);
                    luggagePrice += areaOneOverweight(passenger, luggageList2);
                } else if(passenger.getMembershipType() == 2 && passenger.getMembershipType() == 3) {
                    luggagePrice += areaOneOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 3, 32));
                } else {
                    log.error("没有这个会员类型");
                }
            } else {
                log.error("没有这个座位类型");
            }

        } else {
            log.error("没有这个乘客类型");
        }

        // 特殊行李
        log.info("Processing special luggage.");
        luggagePrice += luggageService.specialLuggage(passenger, specialLuggage);

        passenger.setLuggagePrice(luggagePrice);
        passengerMapper.updateById(passenger);
    }

    /**
     * 区域二计算普通行李价格（超重或者不超重，行李数量）
     * @param passenger
     * @param luggageList
     * @return
     */
    @Override
    public double areaTwoOverweight(Passenger passenger, List<Luggage> luggageList) {
        // 统计没超重超尺寸的行李数量
        double luggagePrice = passengerMapper.selectById(passenger.getPassId()).getLuggagePrice();

        if(passenger.getMembershipType() > -1 && passenger.getMembershipType() < 3) {
            if(luggageList.size() == 0) {
                luggagePrice += 0;
            } else if (luggageList.size() == 1) {
                luggagePrice += 1100;
            } else if (luggageList.size() == 2) {
                luggagePrice += 2200;
            } else if (luggageList.size() > 3) {
                luggagePrice += (2200 + 1590 * (luggageList.size() - 2));
            }

            for(Luggage luggage : luggageList) {
                double totalSize = luggage.getHeight() + luggage.getWidth() + luggage.getLength();

                if ((luggage.getWeight() > 23 && luggage.getWeight() <= 28) && totalSize <= 158) {
                    luggagePrice += 280;
                } else if ((luggage.getWeight() > 28 && totalSize <= 158) || (luggage.getWeight() <= 23 && totalSize > 158)) {
                    luggagePrice += 690;
                } else if (luggage.getWeight() > 23 && totalSize > 158) {
                    luggagePrice += 1100;
                }
            }

        } else if(passenger.getMembershipType() > 2 && passenger.getMembershipType() < 5) {
            if(luggageList.size() == 0) {
                luggagePrice += 0;
            } else if (luggageList.size() == 1) {
                luggagePrice += 1100;
            } else if (luggageList.size() == 2) {
                luggagePrice += 2200;
            } else if (luggageList.size() > 3) {
                luggagePrice += (2200 + 1590 * (luggageList.size() - 2));
            }

            for(Luggage luggage : luggageList) {
                double totalSize = luggage.getHeight() + luggage.getWidth() + luggage.getLength();

                if(totalSize > 158 && totalSize <= 203) {
                    luggagePrice += 690;
                }
            }
        } else {
            log.error("没有这个会员类型");
        }

        passenger.setLuggagePrice(luggagePrice);
        return luggagePrice;
    }

    /**
     * 区域二行李总价格计算
     * @param passenger
     */
    @Override
    public void areaTwo(Passenger passenger) {
        //每一次算一个乘客的行李价格要先对数据库置0;
        passengerService.updatePriceById(passenger.getPassId());
        Passenger passenger1 = passengerMapper.selectById(passenger.getPassId());
        double luggagePrice = passenger1.getLuggagePrice();

        // 计算该乘客满足条件的行李
        List<Luggage> luggageList3 = luggageService.specialLuggageLimit(passenger);
        List<Luggage> specialLuggage = luggageService.specialLuggage(luggageList3);

        List<Luggage> luggageList = luggageService.luggageLimit(passenger);
        List<Luggage> ordinaryLuggage = luggageService.ordinaryLuggage(luggageList);

        // 普通行李
        if(passenger.getPassType() == 0) {
            if(passenger.getSeatType() < 3) {
                if(passenger.getMembershipType() == 0) {
                    luggagePrice += areaTwoOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 1, 23));
                } else if(passenger.getMembershipType() > 0 && passenger.getMembershipType() < 4) {
                    luggagePrice += areaTwoOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 2, 23));
                } else {
                    log.error("没有这个会员类型");
                }
            } else if(passenger.getSeatType() >= 3) {
                if(passenger.getMembershipType() == 0) {
                    luggagePrice += areaTwoOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 1, 23));
                } else if (passenger.getMembershipType() == 1) {
                    luggagePrice += areaTwoOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 2, 23));
                } else if(passenger.getMembershipType() ==2 && passenger.getMembershipType() == 3) {
                    List<Luggage> luggageList1 = luggageService.reomveFreeLuggage(ordinaryLuggage, 1, 23);
                    List<Luggage> luggageList2 = luggageService.reomveFreeLuggage(ordinaryLuggage, 1, 32);
                    luggagePrice += areaTwoOverweight(passenger, luggageList2);
                } else {
                    log.error("没有这个会员类型");
                }
            } else {
                log.error("没有这个座位类型");
            }
        } else if(passenger.getPassType() == 1) {
            if(passenger.getSeatType() == 0) {
                if(passenger.getMembershipType() == 0) {
                    luggagePrice += areaTwoOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 2, 23));
                } else if(passenger.getMembershipType() > 0 && passenger.getMembershipType() < 4) {
                    luggagePrice += areaTwoOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 3, 23));
                } else {
                    log.error("没有这个会员类型");
                }
            } else if(passenger.getSeatType() == 1 || passenger.getSeatType() == 2) {
                if(passenger.getMembershipType() == 0) {
                    luggagePrice += areaTwoOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 2, 23));
                } else if(passenger.getMembershipType() > 0 && passenger.getMembershipType() < 4) {
                    luggagePrice += areaTwoOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 2, 23));
                } else {
                    log.error("没有这个会员类型");
                }
            } else if(passenger.getSeatType() == 3 || passenger.getSeatType() == 4) {
                if(passenger.getMembershipType() == 0) {
                    luggagePrice += areaTwoOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 2, 32));
                } else if (passenger.getMembershipType() == 1) {
                    List<Luggage> luggageList1 = luggageService.reomveFreeLuggage(ordinaryLuggage, 1, 23);
                    List<Luggage> luggageList2 = luggageService.reomveFreeLuggage(ordinaryLuggage, 2, 32);
                    luggagePrice += areaTwoOverweight(passenger, luggageList2);
                } else if(passenger.getMembershipType() == 2 && passenger.getMembershipType() == 3) {
                    luggagePrice += areaTwoOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 3, 32));
                } else {
                    log.error("没有这个会员类型");
                }
            } else {
                log.error("没有这个座位类型");
            }

        } else {
            log.error("没有这个乘客类型");
        }

        // 特殊行李
        log.info("Processing special luggage.");
        luggagePrice += luggageService.specialLuggage(passenger, specialLuggage);

        passenger.setLuggagePrice(luggagePrice);
        passengerMapper.updateById(passenger);
    }

    /**
     * 区域三计算普通行李价格（超重或者不超重，行李数量）
     * @param passenger
     * @param luggageList
     * @return
     */
    @Override
    public double areaThreeOverweight(Passenger passenger, List<Luggage> luggageList) {
        // 统计没超重超尺寸的行李数量
        double luggagePrice = passengerMapper.selectById(passenger.getPassId()).getLuggagePrice();

        if(passenger.getMembershipType() > -1 && passenger.getMembershipType() < 3) {
            if(luggageList.size() == 0) {
                luggagePrice += 0;
            } else if (luggageList.size() == 1) {
                luggagePrice += 1170;
            } else if (luggageList.size() == 2) {
                luggagePrice += 2340;
            } else if (luggageList.size() > 3) {
                luggagePrice += (2340 + 1590 * (luggageList.size() - 2));
            }

            for(Luggage luggage : luggageList) {
                double totalSize = luggage.getHeight() + luggage.getWidth() + luggage.getLength();

                // TODO 优化
                if ((luggage.getWeight() > 23 && luggage.getWeight() <= 28) && totalSize <= 158) {
                    luggagePrice += 520;
                } else if (luggage.getWeight() > 28 && totalSize <= 158) {
                    luggagePrice += 520;
                } else if (luggage.getWeight() <= 23 && totalSize > 158) {
                    luggagePrice += 520;
                } else if (luggage.getWeight() > 23 && totalSize > 158) {
                    luggagePrice += 520;
                }
            }

        } else if(passenger.getMembershipType() > 2 && passenger.getMembershipType() < 5) {
            if(luggageList.size() == 0) {
                luggagePrice += 0;
            } else if (luggageList.size() == 1) {
                luggagePrice += 1170;
            } else if (luggageList.size() == 2) {
                luggagePrice += 2340;
            } else if (luggageList.size() > 3) {
                luggagePrice += (2340 + 1590 * (luggageList.size() - 2));
            }

            for(Luggage luggage : luggageList) {
                double totalSize = luggage.getHeight() + luggage.getWidth() + luggage.getLength();

                if(totalSize > 158 && totalSize <= 203) {
                    luggagePrice += 520;
                }
            }
        } else {
            log.error("没有这个会员类型");
        }

        passenger.setLuggagePrice(luggagePrice);
        return luggagePrice;
    }

    /**
     * 区域三行李总价格计算
     * @param passenger
     */
    @Override
    public void areaThree(Passenger passenger) {
        //每一次算一个乘客的行李价格要先对数据库置0;
        passengerService.updatePriceById(passenger.getPassId());
        Passenger passenger1 = passengerMapper.selectById(passenger.getPassId());
        double luggagePrice = passenger1.getLuggagePrice();

        // 计算该乘客满足条件的行李
        List<Luggage> luggageList3 = luggageService.specialLuggageLimit(passenger);
        List<Luggage> specialLuggage = luggageService.specialLuggage(luggageList3);

        List<Luggage> luggageList = luggageService.luggageLimit(passenger);
        List<Luggage> ordinaryLuggage = luggageService.ordinaryLuggage(luggageList);

        // 普通行李
        if(passenger.getPassType() == 0) {
            if(passenger.getSeatType() < 3) {
                if(passenger.getMembershipType() == 0) {
                    luggagePrice += areaThreeOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 1, 23));
                } else if(passenger.getMembershipType() > 0 && passenger.getMembershipType() < 4) {
                    luggagePrice += areaThreeOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 2, 23));
                } else {
                    log.error("没有这个会员类型");
                }
            } else if(passenger.getSeatType() >= 3) {
                if(passenger.getMembershipType() == 0) {
                    luggagePrice += areaThreeOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 1, 23));
                } else if (passenger.getMembershipType() == 1) {
                    luggagePrice += areaThreeOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 2, 23));
                } else if(passenger.getMembershipType() ==2 && passenger.getMembershipType() == 3) {
                    List<Luggage> luggageList1 = luggageService.reomveFreeLuggage(ordinaryLuggage, 1, 23);
                    List<Luggage> luggageList2 = luggageService.reomveFreeLuggage(ordinaryLuggage, 1, 32);
                    luggagePrice += areaThreeOverweight(passenger, luggageList2);
                } else {
                    log.error("没有这个会员类型");
                }
            } else {
                log.error("没有这个座位类型");
            }
        } else if(passenger.getPassType() == 1) {
            if(passenger.getSeatType() == 0) {
                if(passenger.getMembershipType() == 0) {
                    luggagePrice += areaThreeOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 1, 23));
                } else if(passenger.getMembershipType() > 0 && passenger.getMembershipType() < 4) {
                    luggagePrice += areaThreeOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 2, 23));
                } else {
                    log.error("没有这个会员类型");
                }
            } else if(passenger.getSeatType() == 1 || passenger.getSeatType() == 2) {
                if(passenger.getMembershipType() == 0) {
                    luggagePrice += areaThreeOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 2, 23));
                } else if(passenger.getMembershipType() > 0 && passenger.getMembershipType() < 4) {
                    luggagePrice += areaThreeOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 2, 23));
                } else {
                    log.error("没有这个会员类型");
                }
            } else if(passenger.getSeatType() == 3 || passenger.getSeatType() == 4) {
                if(passenger.getMembershipType() == 0) {
                    luggagePrice += areaThreeOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 2, 32));
                } else if (passenger.getMembershipType() == 1) {
                    List<Luggage> luggageList1 = luggageService.reomveFreeLuggage(ordinaryLuggage, 1, 23);
                    List<Luggage> luggageList2 = luggageService.reomveFreeLuggage(ordinaryLuggage, 2, 32);
                    luggagePrice += areaThreeOverweight(passenger, luggageList2);
                } else if(passenger.getMembershipType() == 2 && passenger.getMembershipType() == 3) {
                    luggagePrice += areaThreeOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 3, 32));
                } else {
                    log.error("没有这个会员类型");
                }
            } else {
                log.error("没有这个座位类型");
            }

        } else {
            log.error("没有这个乘客类型");
        }

        // 特殊行李
        log.info("Processing special luggage.");
        luggagePrice += luggageService.specialLuggage(passenger, specialLuggage);

        passenger.setLuggagePrice(luggagePrice);
        passengerMapper.updateById(passenger);
    }

    /**
     * 区域四计算普通行李价格（超重或者不超重，行李数量）
     * @param passenger
     * @param luggageList
     * @return
     */
    @Override
    public double areaFourOverweight(Passenger passenger, List<Luggage> luggageList) {
        // 统计没超重超尺寸的行李数量
        double luggagePrice = passengerMapper.selectById(passenger.getPassId()).getLuggagePrice();

        if(passenger.getMembershipType() > -1 && passenger.getMembershipType() < 3) {
            if(luggageList.size() == 0) {
                luggagePrice += 0;
            } else if (luggageList.size() == 1) {
                luggagePrice += 1380;
            } else if (luggageList.size() == 2) {
                luggagePrice += 2760;
            } else if (luggageList.size() > 3) {
                luggagePrice += (2760 + 1590 * (luggageList.size() - 2));
            }

            for(Luggage luggage : luggageList) {
                double totalSize = luggage.getHeight() + luggage.getWidth() + luggage.getLength();

                if ((luggage.getWeight() > 23 && luggage.getWeight() <= 28) && totalSize <= 158) {
                    luggagePrice += 690;
                } else if (luggage.getWeight() > 28 && totalSize <= 158) {
                    luggagePrice += 1040;
                } else if (luggage.getWeight() <= 23 && totalSize > 158) {
                    luggagePrice += 1040;
                } else if (luggage.getWeight() > 23 && totalSize > 158) {
                    luggagePrice += 2050;
                }
            }

        } else if(passenger.getMembershipType() > 2 && passenger.getMembershipType() < 5) {
            if(luggageList.size() == 0) {
                luggagePrice += 0;
            } else if (luggageList.size() == 1) {
                luggagePrice += 1380;
            } else if (luggageList.size() == 2) {
                luggagePrice += 2760;
            } else if (luggageList.size() > 3) {
                luggagePrice += (2760 + 1590 * (luggageList.size() - 2));
            }

            for(Luggage luggage : luggageList) {
                double totalSize = luggage.getHeight() + luggage.getWidth() + luggage.getLength();

                if(totalSize > 158 && totalSize <= 203) {
                    luggagePrice += 1040;
                }
            }
        } else {
            log.error("没有这个会员类型");
        }

        passenger.setLuggagePrice(luggagePrice);
        return luggagePrice;
    }

    /**
     * 区域四行李总价格计算
     * @param passenger
     */
    @Override
    public void areaFour(Passenger passenger) {
        //每一次算一个乘客的行李价格要先对数据库置0;
        passengerService.updatePriceById(passenger.getPassId());
        Passenger passenger1 = passengerMapper.selectById(passenger.getPassId());
        double luggagePrice = passenger1.getLuggagePrice();

        // 计算该乘客满足条件的行李
        List<Luggage> luggageList3 = luggageService.specialLuggageLimit(passenger);
        List<Luggage> specialLuggage = luggageService.specialLuggage(luggageList3);

        List<Luggage> luggageList = luggageService.luggageLimit(passenger);
        List<Luggage> ordinaryLuggage = luggageService.ordinaryLuggage(luggageList);

        // 普通行李
        if(passenger.getPassType() == 0) {
            if(passenger.getSeatType() < 3) {
                if(passenger.getMembershipType() == 0) {
                    luggagePrice += areaFourOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 1, 23));
                } else if(passenger.getMembershipType() > 0 && passenger.getMembershipType() < 4) {
                    luggagePrice += areaFourOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 2, 23));
                } else {
                    log.error("没有这个会员类型");
                }
            } else if(passenger.getSeatType() >= 3) {
                if(passenger.getMembershipType() == 0) {
                    luggagePrice += areaFourOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 1, 23));
                } else if (passenger.getMembershipType() == 1) {
                    luggagePrice += areaFourOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 2, 23));
                } else if(passenger.getMembershipType() ==2 && passenger.getMembershipType() == 3) {
                    List<Luggage> luggageList1 = luggageService.reomveFreeLuggage(ordinaryLuggage, 1, 23);
                    List<Luggage> luggageList2 = luggageService.reomveFreeLuggage(ordinaryLuggage, 1, 32);
                    luggagePrice += areaFourOverweight(passenger, luggageList2);
                } else {
                    log.error("没有这个会员类型");
                }
            } else {
                log.error("没有这个座位类型");
            }
        } else if(passenger.getPassType() == 1) {
            if(passenger.getSeatType() == 0) {
                if(passenger.getMembershipType() == 0) {
                    luggagePrice += areaFourOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 1, 23));
                } else if(passenger.getMembershipType() > 0 && passenger.getMembershipType() < 4) {
                    luggagePrice += areaFourOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 2, 23));
                } else {
                    log.error("没有这个会员类型");
                }
            } else if(passenger.getSeatType() == 1 || passenger.getSeatType() == 2) {
                if(passenger.getMembershipType() == 0) {
                    luggagePrice += areaFourOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 2, 23));
                } else if(passenger.getMembershipType() > 0 && passenger.getMembershipType() < 4) {
                    luggagePrice += areaFourOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 2, 23));
                } else {
                    log.error("没有这个会员类型");
                }
            } else if(passenger.getSeatType() == 3 || passenger.getSeatType() == 4) {
                if(passenger.getMembershipType() == 0) {
                    luggagePrice += areaFourOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 2, 32));
                } else if (passenger.getMembershipType() == 1) {
                    List<Luggage> luggageList1 = luggageService.reomveFreeLuggage(ordinaryLuggage, 1, 23);
                    List<Luggage> luggageList2 = luggageService.reomveFreeLuggage(ordinaryLuggage, 2, 32);
                    luggagePrice += areaFourOverweight(passenger, luggageList2);
                } else if(passenger.getMembershipType() == 2 && passenger.getMembershipType() == 3) {
                    luggagePrice += areaFourOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 3, 32));
                } else {
                    log.error("没有这个会员类型");
                }
            } else {
                log.error("没有这个座位类型");
            }

        } else {
            log.error("没有这个乘客类型");
        }

        // 特殊行李
        log.info("Processing special luggage.");
        luggagePrice += luggageService.specialLuggage(passenger, specialLuggage);

        passenger.setLuggagePrice(luggagePrice);
        passengerMapper.updateById(passenger);
    }

    /**
     * 区域五计算普通行李价格（超重或者不超重，行李数量）
     * @param passenger
     * @param luggageList
     * @return
     */
    @Override
    public double areaFiveOverweight(Passenger passenger, List<Luggage> luggageList) {
        // 统计没超重超尺寸的行李数量
        double luggagePrice = passengerMapper.selectById(passenger.getPassId()).getLuggagePrice();

        if(passenger.getMembershipType() > -1 && passenger.getMembershipType() < 3) {
            if(luggageList.size() == 0) {
                luggagePrice += 0;
            } else if (luggageList.size() == 1) {
                luggagePrice += 830;
            } else if (luggageList.size() == 2) {
                luggagePrice += 1930;
            } else if (luggageList.size() > 3) {
                luggagePrice += (1930 + 1590 * (luggageList.size() - 2));
            }

            for(Luggage luggage : luggageList) {
                double totalSize = luggage.getHeight() + luggage.getWidth() + luggage.getLength();

                // TODO 优化
                if ((luggage.getWeight() > 23 && luggage.getWeight() <= 28) && totalSize <= 158) {
                    luggagePrice += 210;
                } else if (luggage.getWeight() > 28 && totalSize <= 158) {
                    luggagePrice += 520;
                } else if (luggage.getWeight() <= 23 && totalSize > 158) {
                    luggagePrice += 520;
                } else if (luggage.getWeight() > 23 && totalSize > 158) {
                    luggagePrice += 830;
                }
            }

        } else if(passenger.getMembershipType() > 2 && passenger.getMembershipType() < 5) {
            if(luggageList.size() == 0) {
                luggagePrice += 0;
            } else if (luggageList.size() == 1) {
                luggagePrice += 830;
            } else if (luggageList.size() == 2) {
                luggagePrice += 1930;
            } else if (luggageList.size() > 3) {
                luggagePrice += (1930 + 1590 * (luggageList.size() - 2));
            }

            for(Luggage luggage : luggageList) {
                double totalSize = luggage.getHeight() + luggage.getWidth() + luggage.getLength();

                if(totalSize > 158 && totalSize <= 203) {
                    luggagePrice += 520;
                }
            }
        } else {
            log.error("没有这个会员类型");
        }

        passenger.setLuggagePrice(luggagePrice);
        return luggagePrice;
    }

    /**
     * 区域五行李总价格计算
     * @param passenger
     */
    @Override
    public void areaFive(Passenger passenger) {
        //每一次算一个乘客的行李价格要先对数据库置0;
        passengerService.updatePriceById(passenger.getPassId());
        Passenger passenger1 = passengerMapper.selectById(passenger.getPassId());
        double luggagePrice = passenger1.getLuggagePrice();

        // 计算该乘客满足条件的行李
        List<Luggage> luggageList3 = luggageService.specialLuggageLimit(passenger);
        List<Luggage> specialLuggage = luggageService.specialLuggage(luggageList3);

        List<Luggage> luggageList = luggageService.luggageLimit(passenger);
        List<Luggage> ordinaryLuggage = luggageService.ordinaryLuggage(luggageList);

        // 普通行李
        if(passenger.getPassType() == 0) {
            if(passenger.getSeatType() < 3) {
                if(passenger.getMembershipType() == 0) {
                    luggagePrice += areaFiveOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 1, 23));
                } else if(passenger.getMembershipType() > 0 && passenger.getMembershipType() < 4) {
                    luggagePrice += areaFiveOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 2, 23));
                } else {
                    log.error("没有这个会员类型");
                }
            } else if(passenger.getSeatType() >= 3) {
                if(passenger.getMembershipType() == 0) {
                    luggagePrice += areaFiveOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 1, 23));
                } else if (passenger.getMembershipType() == 1) {
                    luggagePrice += areaFiveOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 2, 23));
                } else if(passenger.getMembershipType() ==2 && passenger.getMembershipType() == 3) {
                    List<Luggage> luggageList1 = luggageService.reomveFreeLuggage(ordinaryLuggage, 1, 23);
                    List<Luggage> luggageList2 = luggageService.reomveFreeLuggage(ordinaryLuggage, 1, 32);
                    luggagePrice += areaFiveOverweight(passenger, luggageList2);
                } else {
                    log.error("没有这个会员类型");
                }
            } else {
                log.error("没有这个座位类型");
            }
        } else if(passenger.getPassType() == 1) {
            if(passenger.getSeatType() == 0) {
                if(passenger.getMembershipType() == 0) {
                    luggagePrice += areaFiveOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 1, 23));
                } else if(passenger.getMembershipType() > 0 && passenger.getMembershipType() < 4) {
                    luggagePrice += areaFiveOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 2, 23));
                } else {
                    log.error("没有这个会员类型");
                }
            } else if(passenger.getSeatType() == 1 || passenger.getSeatType() == 2) {
                if(passenger.getMembershipType() == 0) {
                    luggagePrice += areaFiveOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 2, 23));
                } else if(passenger.getMembershipType() > 0 && passenger.getMembershipType() < 4) {
                    luggagePrice += areaFiveOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 2, 23));
                } else {
                    log.error("没有这个会员类型");
                }
            } else if(passenger.getSeatType() == 3 || passenger.getSeatType() == 4) {
                if(passenger.getMembershipType() == 0) {
                    luggagePrice += areaFiveOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 2, 32));
                } else if (passenger.getMembershipType() == 1) {
                    List<Luggage> luggageList1 = luggageService.reomveFreeLuggage(ordinaryLuggage, 1, 23);
                    List<Luggage> luggageList2 = luggageService.reomveFreeLuggage(ordinaryLuggage, 2, 32);
                    luggagePrice += areaFiveOverweight(passenger, luggageList2);
                } else if(passenger.getMembershipType() == 2 && passenger.getMembershipType() == 3) {
                    luggagePrice += areaFiveOverweight(passenger, luggageService.reomveFreeLuggage(ordinaryLuggage, 3, 32));
                } else {
                    log.error("没有这个会员类型");
                }
            } else {
                log.error("没有这个座位类型");
            }

        } else {
            log.error("没有这个乘客类型");
        }

        // 特殊行李
        log.info("Processing special luggage.");
        luggagePrice += luggageService.specialLuggage(passenger, specialLuggage);

        passenger.setLuggagePrice(luggagePrice);
        passengerMapper.updateById(passenger);
    }

}
