package com.example.calculate.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;
import lombok.Data;

@Data
@TableName("`luggage`")
@Builder
public class Luggage {
    // 使用mybatisPlus中的selectById必须标记主键
    @TableId
    private Long luggageId;

    private Long passId;
    // 0表示普通行李，1表示特殊行李
    private Integer type;
    // 0-8分别代表特殊行李9类，如果为国内航班就只有5类
    private Integer specialType;

    private double weight;

    private double length;

    private double width;

    private double height;


}
