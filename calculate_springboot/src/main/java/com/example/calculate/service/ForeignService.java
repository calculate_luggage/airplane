package com.example.calculate.service;

import com.example.calculate.pojo.Luggage;
import com.example.calculate.pojo.Passenger;

import java.util.List;

public interface ForeignService {

    public double areaOneOverweight(Passenger passenger, List<Luggage> luggageList);

    public void areaOne(Passenger passenger);

    double areaTwoOverweight(Passenger passenger, List<Luggage> luggageList);

    public void areaTwo(Passenger passenger);

    double areaThreeOverweight(Passenger passenger, List<Luggage> luggageList);

    public void areaThree(Passenger passenger);

    double areaFourOverweight(Passenger passenger, List<Luggage> luggageList);

    public void areaFour(Passenger passenger);

    double areaFiveOverweight(Passenger passenger, List<Luggage> luggageList);

    public void areaFive(Passenger passenger);

}
