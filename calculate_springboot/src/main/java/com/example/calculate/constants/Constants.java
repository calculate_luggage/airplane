package com.example.calculate.constants;

public class Constants {

    public static final int ECONOMY_TICKET_PRICE = 666;
    public static final double OVERWEIGHT_FARE_RATIO = 0.015;
}
