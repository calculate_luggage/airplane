package com.example.calculate.service;

import com.example.calculate.pojo.Luggage;
import com.example.calculate.pojo.Passenger;

import java.util.List;


public interface PassengerService {

    /**
     * 将乘客行李价格置0
     * @param passId
     */
    public void updatePriceById(Long passId);



}
