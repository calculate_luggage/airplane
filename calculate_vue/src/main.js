import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'primevue/resources/themes/aura-light-green/theme.css'
import PrimeVue from 'primevue/config'
import axios from 'axios'

const app = createApp(App)
app.config.globalProperties.$http = axios
app.use(store).use(router).use(PrimeVue).mount('#app')
// app.config.globalProperties.axios = axios
