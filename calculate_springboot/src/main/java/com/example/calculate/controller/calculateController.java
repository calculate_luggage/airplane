package com.example.calculate.controller;

import com.example.calculate.mapper.LuggageMapper;
import com.example.calculate.mapper.PassengerMapper;
import com.example.calculate.pojo.Luggage;
import com.example.calculate.pojo.Passenger;
import com.example.calculate.service.impl.DomesticServiceImpl;
import com.example.calculate.service.impl.ForeignServiceImpl;
import com.example.calculate.service.impl.LuggageServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/cal")
public class calculateController {

    @Autowired
    private ForeignServiceImpl foreignService;

    @Autowired
    private DomesticServiceImpl domesticService;

    @Autowired
    private PassengerMapper passengerMapper;

    @Autowired
    private LuggageMapper luggageMapper;

    @Autowired
    private LuggageServiceImpl luggageService;

    /**
     * 计算该乘客行李托运费用
     * @param passId
     * @return
     */
    @RequestMapping("/calPrice")
    public double calPrice(@RequestParam Long passId) {
        log.info("----------------进入该方法了-----------------");

        Passenger passenger = passengerMapper.selectById(passId);

        if(passenger.getArea() == 0) {
            domesticService.totalDomestic(passenger);
        } else if(passenger.getArea() == 1) {
            foreignService.areaOne(passenger);
        } else if(passenger.getArea() == 2) {
            foreignService.areaTwo(passenger);
        } else if(passenger.getArea() == 3) {
            foreignService.areaThree(passenger);
        } else if(passenger.getArea() == 4) {
            foreignService.areaFour(passenger);
        } else if(passenger.getArea() == 5) {
            foreignService.areaFive(passenger);
        }
        // 重新加载最新的数据
        passenger = passengerMapper.selectById(passId);

        return passenger.getLuggagePrice();
    }

    /**
     * 添加行李
     * @param luggage
     */
    @RequestMapping(value = "/addLuggage", method = RequestMethod.POST)
    public void addLuggage(@RequestBody Luggage luggage) {
        System.out.println("------------进入该添加行李方法了-----------");
        luggageMapper.insert(luggage);
        System.out.println("------------添加行李方法成功-----------");
    }

    @RequestMapping("/getLuggages")
    public List<Luggage> getLuggages(@RequestParam Long passId) {
        System.out.println("------------进入该获取行李方法了-----------");
        Passenger passenger = passengerMapper.selectById(passId);
        List<Luggage> luggageList = luggageService.getLuggageList(passenger);
        System.out.println("------------获取行李方法成功-----------");

        return luggageList;
    }

    @RequestMapping("/getPassengers")
    public List<Passenger> getPassengers() {
        System.out.println("------------进入该获取乘客列表方法了-----------");
        List<Passenger> passengers = passengerMapper.selectList(null);
        System.out.println("------------获取乘客列表方法成功-----------");

        return passengers;
    }

    @RequestMapping("/deleteLuggage")
    public void deleteLuggage(@RequestParam Long luggageId) {
        System.out.println("------------进入删除行李方法了-----------");
        luggageService.deleteLuggage(luggageId);
        System.out.println("------------删除行李方法成功-----------");

    }


}
