import { createRouter, createWebHistory } from 'vue-router'
import LuggageForm from '../views/LuggageForm.vue'

const routes = [
  {
    path: '/',
    name: 'luggage',
    component: LuggageForm
  }

]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
