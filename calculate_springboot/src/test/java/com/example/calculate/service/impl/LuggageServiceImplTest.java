package com.example.calculate.service.impl;

import com.example.calculate.mapper.LuggageMapper;
import com.example.calculate.mapper.PassengerMapper;
import com.example.calculate.pojo.Luggage;
import com.example.calculate.pojo.Passenger;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@Slf4j
@SpringBootTest
class LuggageServiceImplTest {

    @Autowired
    private LuggageServiceImpl luggageService;

    @Test
    void specialLuggage() {
        log.info("---------------specialLuggage测试开始------------------");
        Passenger passenger = Passenger.builder()
                .economyTicketPrice(666)
                .luggagePrice(0)
                .area(0)
                .membershipType(1)
                .passId(1L)
                .passType(1)
                .seatType(1).build();
        System.out.println(passenger);

        Luggage luggage = Luggage.builder()
                .luggageId(1L)
                .height(34)
                .length(45)
                .passId(1L)
                .specialType(1)
                .type(1)
                .weight(23)
                .width(56)
                .build();
        Luggage luggage2 = Luggage.builder()
                .luggageId(1L)
                .height(34)
                .length(67)
                .passId(1L)
                .specialType(1)
                .type(1)
                .weight(27)
                .width(56)
                .build();

        List<Luggage> luggageList = new ArrayList<>();
        luggageList.add(luggage);
        luggageList.add(luggage2);
        double specialLuggagePrice = luggageService.specialLuggage(passenger, luggageList);
        System.out.println("specialLuggagePrice测试结果为：" + specialLuggagePrice);
        log.info("---------------specialLuggage测试结束------------------");

    }

    @Test
    void getLuggageList() {
        log.info("---------------specialLuggage测试开始------------------");
        Passenger passenger = Passenger.builder()
                .economyTicketPrice(666)
                .luggagePrice(0)
                .area(0)
                .membershipType(1)
                .passId(1L)
                .passType(1)
                .seatType(1).build();

        List<Luggage> luggageList = luggageService.getLuggageList(passenger);
        luggageList.stream().forEach(luggage -> System.out.println(luggage));
        log.info("---------------specialLuggage测试结束------------------");

    }

    @Test
    void totalWeight() {
        log.info("---------------totalWeight测试开始------------------");
        Luggage luggage = Luggage.builder()
                .luggageId(1L)
                .height(34)
                .length(45)
                .passId(1L)
                .specialType(1)
                .type(1)
                .weight(23)
                .width(56)
                .build();
        Luggage luggage2 = Luggage.builder()
                .luggageId(1L)
                .height(34)
                .length(67)
                .passId(1L)
                .specialType(1)
                .type(1)
                .weight(27)
                .width(56)
                .build();

        List<Luggage> luggageList = new ArrayList<>();
        luggageList.add(luggage);
        luggageList.add(luggage2);
        double totalWeight = luggageService.totalWeight(luggageList);
        System.out.println("totalWeight的测试结果为" + totalWeight);
        log.info("---------------totalWeight测试结束------------------");

    }

    @Test
    void testSpecialLuggage() {
        log.info("---------------testSpecialLuggage测试开始------------------");
        Luggage luggage = Luggage.builder()
                .luggageId(1L)
                .height(34)
                .length(45)
                .passId(1L)
                .specialType(1)
                .type(1)
                .weight(23)
                .width(56)
                .build();
        Luggage luggage2 = Luggage.builder()
                .luggageId(1L)
                .height(34)
                .length(67)
                .passId(1L)
                .specialType(1)
                .type(1)
                .weight(27)
                .width(56)
                .build();

        List<Luggage> luggageList = new ArrayList<>();
        luggageList.add(luggage);
        luggageList.add(luggage2);
        List<Luggage> luggageList1 = luggageService.specialLuggage(luggageList);
        luggageList1.stream().forEach(luggage1 -> System.out.println(luggage1));
        log.info("---------------testSpecialLuggage测试结束------------------");

    }

    @Test
    void ordinaryLuggage() {
        log.info("---------------ordinaryLuggage测试开始------------------");
        Luggage luggage = Luggage.builder()
                .luggageId(1L)
                .height(34)
                .length(45)
                .passId(1L)
                .specialType(1)
                .type(1)
                .weight(23)
                .width(56)
                .build();
        Luggage luggage2 = Luggage.builder()
                .luggageId(1L)
                .height(34)
                .length(67)
                .passId(1L)
                .specialType(1)
                .type(1)
                .weight(27)
                .width(56)
                .build();

        List<Luggage> luggageList = new ArrayList<>();
        luggageList.add(luggage);
        luggageList.add(luggage2);
        List<Luggage> luggageList1 = luggageService.ordinaryLuggage(luggageList);
        luggageList1.stream().forEach(luggage1 -> System.out.println(luggage1));
        log.info("---------------ordinaryLuggage测试结束------------------");

    }

    @Test
    void luggageLimit() {
        log.info("---------------luggageLimit测试开始------------------");
        Passenger passenger = Passenger.builder()
                .economyTicketPrice(666)
                .luggagePrice(0)
                .area(0)
                .membershipType(1)
                .passId(1L)
                .passType(1)
                .seatType(1).build();
        System.out.println(passenger);

        List<Luggage> luggageList = luggageService.luggageLimit(passenger);
        luggageList.stream().forEach(luggage1 -> System.out.println(luggage1));
        log.info("---------------luggageLimit测试结束------------------");


    }

    @Test
    void reomveFreeLuggage() {
        log.info("---------------reomveFreeLuggage测试开始------------------");
        Luggage luggage = Luggage.builder()
                .luggageId(1L)
                .height(34)
                .length(45)
                .passId(1L)
                .specialType(1)
                .type(1)
                .weight(23)
                .width(56)
                .build();
        Luggage luggage2 = Luggage.builder()
                .luggageId(1L)
                .height(34)
                .length(209)
                .passId(1L)
                .specialType(1)
                .type(1)
                .weight(27)
                .width(56)
                .build();
        Luggage luggage3 = Luggage.builder()
                .luggageId(1L)
                .height(34)
                .length(67)
                .passId(1L)
                .specialType(1)
                .type(1)
                .weight(28)
                .width(56)
                .build();
        Luggage luggage4 = Luggage.builder()
                .luggageId(1L)
                .height(34)
                .length(67)
                .passId(1L)
                .specialType(1)
                .type(1)
                .weight(45)
                .width(56)
                .build();

        List<Luggage> luggageList = new ArrayList<>();
        luggageList.add(luggage);
        luggageList.add(luggage2);
        luggageList.add(luggage3);
        luggageList.add(luggage4);

        List<Luggage> luggageList1 = luggageService.reomveFreeLuggage(luggageList, 1, 27);
        luggageList1.stream().forEach(luggage1 -> System.out.println(luggage1));
        log.info("---------------reomveFreeLuggage测试结束------------------");

    }

    @Test
    void calculateTotalSize() {
        log.info("---------------calculateTotalSize测试开始------------------");
        Luggage luggage = Luggage.builder()
                .luggageId(1L)
                .height(34)
                .length(45)
                .passId(1L)
                .specialType(1)
                .type(1)
                .weight(23)
                .width(56)
                .build();

        double calculateTotalSize = luggageService.calculateTotalSize(luggage);
        System.out.println("calculateTotalSize的测试结果为：" + calculateTotalSize);
        log.info("---------------calculateTotalSize测试结束------------------");
    }

    @Test
    void luggageOver() {
        log.info("---------------luggageOver测试开始------------------");
        Luggage luggage = Luggage.builder()
                .luggageId(1L)
                .height(34)
                .length(45)
                .passId(1L)
                .specialType(1)
                .type(1)
                .weight(23)
                .width(56)
                .build();
        Luggage luggage2 = Luggage.builder()
                .luggageId(1L)
                .height(34)
                .length(209)
                .passId(1L)
                .specialType(1)
                .type(1)
                .weight(27)
                .width(56)
                .build();
        Luggage luggage3 = Luggage.builder()
                .luggageId(1L)
                .height(34)
                .length(67)
                .passId(1L)
                .specialType(1)
                .type(1)
                .weight(28)
                .width(56)
                .build();
        Luggage luggage4 = Luggage.builder()
                .luggageId(1L)
                .height(34)
                .length(67)
                .passId(1L)
                .specialType(1)
                .type(1)
                .weight(45)
                .width(56)
                .build();

        List<Luggage> luggageList = new ArrayList<>();
        luggageList.add(luggage);
        luggageList.add(luggage2);
        luggageList.add(luggage3);
        luggageList.add(luggage4);
        List<Luggage> luggageList1 = luggageService.luggageOver(luggageList, 27);
        luggageList1.stream().forEach(luggage1 -> System.out.println(luggage1));
        log.info("---------------luggageOver测试结束------------------");

    }
}