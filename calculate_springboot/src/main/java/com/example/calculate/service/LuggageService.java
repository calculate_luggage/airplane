package com.example.calculate.service;

import com.example.calculate.pojo.Luggage;
import com.example.calculate.pojo.Passenger;

import java.util.List;

public interface LuggageService {


//    public List<Luggage> foreignSpecial(List<Luggage> luggageList);
    /**
     * 特殊行李的限制
     *
     * @param passenger
     * @return
     */
    List<Luggage> specialLuggageLimit(Passenger passenger);
    double specialLuggage(Passenger passenger, List<Luggage> specialLuggage);

    /**
     * 查询该乘客的所有行李
     * @param passenger
     * @return
     */
    public List<Luggage> getLuggageList(Passenger passenger);

    /**
     * 计算行李总重量
     * @param luggageList
     * @return
     */
    public double totalWeight(List<Luggage> luggageList);

    /**
     * 得到特殊行李
     * @param luggageList
     * @return
     */
    public List<Luggage> specialLuggage(List<Luggage> luggageList);


    /**
     * 得到普通行李
     * @param luggageList
     * @return
     */
    public List<Luggage> ordinaryLuggage(List<Luggage> luggageList);

    /**
     * 得到满足条件的行李，尺寸，重量限制
     * @param passenger
     * @return
     */
    public List<Luggage> luggageLimit(Passenger passenger);

    /**
     * 获取剔除免费托运行李之后的行李列表
     * @param luggageList
     * @param freeCount
     * @return
     */
    public List<Luggage> reomveFreeLuggage(List<Luggage> luggageList, int freeCount, double weight);

    /**
     * 统计行李长宽高尺寸
     * @param luggage
     * @return
     */
    public double calculateTotalSize(Luggage luggage);

    /**
     * 计算超过限制的行李列表
     * @param luggageList
     * @param weight
     * @return
     */
    public List<Luggage> luggageOver(List<Luggage> luggageList, double weight);

    /**
     * 删除行李通过id
     * @param luggageId
     */
    void deleteLuggage(Long luggageId);


}
