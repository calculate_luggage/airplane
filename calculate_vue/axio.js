import axios from 'axios'

axios.defaults.baseURL = '/api' // 设置基础URL
axios.defaults.timeout = 100000

export default axios
