package com.example.calculate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.example.calculate.mapper.PassengerMapper;
import com.example.calculate.pojo.Luggage;
import com.example.calculate.pojo.Passenger;
import com.example.calculate.service.PassengerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PassengerServiceImpl implements PassengerService {

    @Autowired
    private PassengerMapper passengerMapper;

    /**
     * 将乘客行李价格置0
     * @param passId
     */
    @Override
    public void updatePriceById(Long passId) {

        Passenger passenger = passengerMapper.selectById(passId);
        passenger.setLuggagePrice(0);
        UpdateWrapper<Passenger> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("pass_id", passId);
        passengerMapper.update(passenger, updateWrapper);

    }
}
